#Jason Santos, Julia Romani, Patrick Rodrigues

import ply.lex as lex
import ply.yacc as yacc
import sys

tokens = [

    'INT',
    'REAL',
    'ID',
    'PLUS',
    'MINUS',
    'DIVIDE',
    'MULTIPLY',
    'EQUALS',
    'POWER',
    'LEFT_PAR',
    'RIGHT_PAR',
    'SEMICOLON',
    'WHILE',
    'IF',
    'ELSE',
    'RL_OP',
    'LEFT_BRACES',
    'RIGHT_BRACES',
    'FLOAT',
    'VAR',
    'INTVAR',
    'COMMA',
    'LEFT_BRACKET',
    'RIGHT_BRACKET'

]

reserved = {
    'while' : 'WHILE',
    'if'    : 'IF',
    'else'  : 'ELSE',
    'real'  : 'REAL',
    'var'   : 'VAR',
    'int'   : 'INTVAR',
}

t_PLUS = r'\+'
t_MINUS = r'\-'
t_MULTIPLY = r'\*'
t_DIVIDE = r'\/'
t_EQUALS = r'\='
t_POWER = r'\^'
t_LEFT_PAR = r'\('
t_RIGHT_PAR = r'\)'
t_SEMICOLON = r'\;'
t_RL_OP = r'==|<>|<=|>=|<|>'
t_LEFT_BRACES = r'\{'
t_RIGHT_BRACES = r'\}'
t_LEFT_BRACKET = r'\['
t_RIGHT_BRACKET = r'\]'
t_COMMA = r'\,'
t_ignore = r' '

def t_FLOAT(t):
    r'\d+\.\d+'
    t.value = float(t.value)
    return t

def t_INT(t):
    r'\d+'
    t.value = int(t.value)
    return t

def t_ID(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    if t.value in reserved:
        t.type = reserved[ t.value ]
    return t

def t_error(t):
    print("Illegal characters!")
    t.lexer.skip(1)

lexer = lex.lex()

precedence = (

    ('left', 'PLUS', 'MINUS'),
    ('left', 'MULTIPLY', 'DIVIDE'),
    ('left', 'POWER')

)

def p_program_p(p):
    '''
     p_program_p : program
                 | program p_program_p
    '''

def p_program(p):
    '''
    program : expression
            | expression_ctrl
            | var_assign
            | VAR var_type var_declaration SEMICOLON
    '''
    print ('')

def p_program_print_error(p):
    '''
    program : error var_type var_declaration SEMICOLON
    '''
    print("Erro: Falta termo VAR na declaração.")

def p_program2_print_error(p):
    '''
    program : VAR var_type var_declaration error
    '''
    print("Erro: Falta ponto-e-virgula na declaração.")

def p_program3_print_error(p):
    '''
    program : VAR error var_declaration SEMICOLON
    '''
    print("Erro: Falta declaração de TIPO.")

def p_var_declaration(p):
    '''
    var_declaration : var_type_id
                    | var_declaration SEMICOLON var_type var_type_id
    '''


def p_var_type_id(p):
    '''
    var_type_id : id_class
                | var_type_id COMMA id_class
    '''

def p_var_type(p):
    '''
    var_type : REAL
             | INTVAR
    '''

def p_id_class(p):
    '''
    id_class : ID
             | ID LEFT_BRACKET INT RIGHT_BRACKET
             | ID LEFT_BRACKET ID RIGHT_BRACKET
    '''

def p_var_assign(p):
    '''
    var_assign : id_class EQUALS expression SEMICOLON
    '''

def p_expression_var(p):
    '''
    expression : id_class
    '''

# Expressions are recursive.
def p_expression(p):
    '''
    expression : expression MULTIPLY expression
               | expression DIVIDE expression
               | expression PLUS expression
               | expression MINUS expression
               | expression POWER expression
    '''
    p[0] = (p[2], p[1], p[3])

def p_expression_int_float(p):
    '''
    expression : INT
               | FLOAT
    '''

def p_expression_par(p):
    '''
    expression : LEFT_PAR expression RIGHT_PAR
    '''

def p_expression_co(p):
    '''
    expression_co : var_assign
                  | expression_ctrl
    '''

def p_expression_c(p):
    '''
    expression_c : expression_co
                 | expression_co expression_c
    '''

def p_expression_bra(p):
    '''
    expression_bra : LEFT_BRACES expression_c RIGHT_BRACES
    '''

def p_expression_rl(p):
    '''
    expression_rl : expression RL_OP expression
    '''

def p_cmd_rl(p):
    '''
    expression_ctrl : WHILE LEFT_PAR expression_rl RIGHT_PAR expression_bra
                    | IF LEFT_PAR expression_rl RIGHT_PAR expression_bra
                    | IF LEFT_PAR expression_rl RIGHT_PAR expression_bra ELSE expression_bra
    '''
    p[0] = p[3]

def p_cmd_rl_print_error(p):
    '''
    expression_ctrl : error LEFT_PAR expression_rl RIGHT_PAR expression_bra
                    | error LEFT_PAR expression_rl RIGHT_PAR expression_bra ELSE expression_bra
    '''
    print("Erro: Falta palavra reservada IF.")

#def p_error(p):
#    print("Illegal character '%s'" % p.value[0])
#    print(p)

parser = yacc.yacc()

with open("program.txt", "r") as f:
    s = f.read().replace('\n',' ')
    parser.parse(s)


'''
while True:
    try:
        s = input('>> ')
    except EOFError:
        break
    parser.parse(s)

'''
